

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json()) 
.then(response => console.log(response))

console.log()
console.log('The item "delectus aut autem" on the list has a status of false')


fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "Created To Do List Item",
			userId:1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))
   

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		title: "Updated To Do List Item",
		status: "Pending",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "02/08/23",
		title: "delectus aut autem",
		status: "Complete",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// DELETE 
 fetch("https://jsonplaceholder.typicode.com/todos/1", 
	 {
	 		 method: "DELETE"
	 }
).then(response => response.json())
.then(json => console.log(json))